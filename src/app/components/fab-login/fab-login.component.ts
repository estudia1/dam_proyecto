// Importar los módulos necesarios
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { LoginComponent } from '../login/login.component';

/**
 * Componente tipo Fab que permite acceder al formulario de Login.
 * Este componente se muestra como un botón flotante en la esquina inferior derecha de la pantalla.
 * Solo se muestra en las páginas de acceso anónimo y cuando el usuario no está logado.
 * Al pulsar el botón, se abre una ventana modal con el componente LoginComponent, que permite al usuario iniciar sesión o registrarse.
 */
@Component({
  selector: 'app-fab-login',
  templateUrl: './fab-login.component.html',
  styleUrls: ['./fab-login.component.scss'],
})
export class FabLoginComponent implements OnInit {
  /**
   * Constructor de la clase
   * @param modalCtrl modalCtrl Controlador de Modal de Ionic, que permite crear y presentar ventanas modales
   *
   */
  constructor(private modalCtrl: ModalController) {}

  /**
   * Método de inicio que se ejecuta cuando el componente se inicializa
   * No hace nada en este caso
   */
  ngOnInit() {}
  /**
   * Método que abre la ventana de Login como un Modal
   * @returns {Promise<void>} Una promesa que se resuelve cuando el modal se presenta
   *
   */
  async loginModal() {
    // Crear el modal con el componente LoginComponent y una clase CSS personalizada
    const modal = await this.modalCtrl.create({
      component: LoginComponent,
      cssClass: 'loginRegisterModal',
    });
    // Presentar el modal y devolver la promesa
    return await modal.present();
  }
}
