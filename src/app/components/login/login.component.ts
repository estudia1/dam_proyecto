import { Component, OnInit } from '@angular/core';

import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ModalController, NavController } from '@ionic/angular';

import { AuthService } from 'src/app/services/auth.service';

import { RegistroComponent } from '../registro/registro.component';
import { Error } from 'src/app/interfaces/errores';
import { Usuario } from 'src/app/interfaces/interfaces';
import { UserService } from 'src/app/services/user.service';

/**
 * Componente que permite al usuario iniciar sesión o registrarse en la aplicación.
 * Este componente se muestra como un formulario con los campos de email y contraseña, y los botones de login y registro.
 * Al pulsar el botón de login, se usa el servicio de AuthService para autenticar al usuario con Firebase Authentication, y se redirige al home si el login es exitoso.
 * Al pulsar el botón de registro, se abre una ventana modal con el componente RegistroComponent, que permite al usuario crear una cuenta con Firebase Authentication.
 * Este componente se usa en la página de login, que es la primera que se muestra al abrir la aplicación si el usuario no está logado.
 */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  /**
   * Propiedad que almacena el formulario de login con los campos de email y contraseña.
   * @type {FormGroup} El formulario de login
   */
  loginForm: FormGroup;

  /**
   * Propiedad que almacena el texto a mostrar si el acceso es correcto.
   * @type {string} El texto de éxito
   */
  successMsg: string = '';

  /**
   * Propiedad que almacena el texto a mostrar si el acceso es erróneo.
   * @type {Error[]} El texto de error
   */
  errorMsg: Error[] = [];
  /**
   * Propiedad que almacena el objeto usuario con la información que se necesite manejar.
   * @type {Usuario} El objeto usuario
   */
  usuario: Usuario;

  /**
   * Constructor de la clase
   * @param authSvc Servicio para el manejo de la Autenticación con Firebase Auth, que permite iniciar sesión, registrarse, cerrar sesión y recuperar contraseña de los usuarios
   * @param userSvc Servicio que maneja la colección 'users' de Firestore, que permite obtener, crear, modificar y eliminar usuarios
   * @param fBuilder Servicio de Angular que permite crear formularios reactivos con validadores
   * @param modalCtrl Controlador de Modal de Ionic, que permite crear y presentar ventanas modales
   * @param navCtrl Controlador de Navegación de Ionic, que permite navegar entre las páginas de la aplicación
   */
  constructor(
    private authSvc: AuthService,
    private userSvc: UserService,
    private fBuilder: FormBuilder,
    private modalCtrl: ModalController,
    private navCtrl: NavController
  ) {}

  /**
   * Método de inicio que se ejecuta cuando el componente se inicializa.
   * Crea el formulario de login con los validadores necesarios para los campos de email y contraseña.
   */
  ngOnInit() {
    // Crear el formulario de login usando el servicio de FormBuilder
    this.loginForm = this.fBuilder.group({
      // Campo de email con validadores de requerido y formato de email
      email: new FormControl(
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),
        ])
      ),
      // Campo de contraseña con validadores de requerido y longitud mínima de 6 caracteres
      password: new FormControl(
        '',
        Validators.compose([Validators.minLength(6), Validators.required])
      ),
    });
  }

  /**
   * Método que realiza el login apoyándose en el servicio Auth y redirecciona al home.
   * Este método usa el servicio de AuthService para iniciar sesión con el email y la contraseña introducidos en el formulario.
   * Si el login es exitoso, se usa el servicio de NavController para navegar a la página de home.
   * También se usa el servicio de UserService para guardar el email del usuario logado.
   * Si el login es erróneo, se muestra un mensaje de error en la consola.
   * @param {any} value Los valores pasados por el formulario de login
   * @returns {void}
   */
  async logIn(value: any) {
    // Usar un bloque try-catch para manejar posibles errores
    try {
      // Iniciar sesión con el email y la contraseña usando el servicio de AuthService
      await this.authSvc.signIn(value);

      // Navegar a la página de home usando el servicio de NavController
      this.navCtrl.navigateRoot('/home');

      // Obtener el email del usuario logado del formulario de login
      const userEmail = this.loginForm.value.email;

      // Guardar el email del usuario logado usando el servicio de UserService
      this.userSvc.setUserEmail(userEmail);

      // Mostrar un mensaje de éxito en la consola con el email del usuario logado
      console.log('logado con', userEmail);
    } catch (error) {
      // Mostrar un mensaje de error en la consola con el error ocurrido
      console.log('Error al iniciar sesión:', error);
    }
  }

  /**
   * Método para la autenticación de Google (NO IMPLEMENTADO).
   * Este método está vacío porque no se ha implementado la funcionalidad de iniciar sesión con Google.
   * @param {void}
   * @returns {void}
   */
  gAuth() {}
  // No hacer nada

  /**
   * Método que muestra un modal para el registro del usuario.
   * Este método usa el controlador de Modal de Ionic para crear y presentar una ventana modal con el componente RegistroComponent, que permite al usuario crear una cuenta con Firebase Authentication.
   * @param {void}
   * @returns {Promise<void>} Una promesa que se resuelve cuando el modal se presenta
   */
  async registroModal() {
    // Crear el modal con el componente RegistroComponent y una clase CSS personalizada usando el controlador de Modal de Ionic
    const modal = await this.modalCtrl.create({
      component: RegistroComponent,
      cssClass: 'loginRegisterModal',
    });
    // Presentar el modal y devolver la promesa que se resuelve cuando el modal se presenta
    return await modal.present();
  }

  /**
   * Método que cierra el propio modal.
   * Este método usa el controlador de Modal de Ionic para cerrar el modal que muestra el formulario de login.
   * @param {void}
   * @returns {void}
   */
  closeModal() {
    // Cerrar el modal usando el controlador de Modal de Ionic
    this.modalCtrl.dismiss();
  }
}
