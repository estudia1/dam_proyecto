import { Component } from '@angular/core';

/**
 * Componente principal de la aplicación.
 * 
 * NO TOCAR!!
 */
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})


export class AppComponent {
  /**
 * Constructor del componente
 * 
 * SE RECOMIENDA NO TOCAR 
 */
  constructor() {

 
     }
}
