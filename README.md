# Proyecto de Astronomía

Este repositorio contiene el código fuente y los recursos para el proyecto de la asignatura PPP de 3DAM, una aplicación social enfocada en el campo de la astronomía con seguimiento de eventos astronómicos.

## Descripción

El objetivo principal de este proyecto es desarrollar una aplicación social enfocada en el campo de la astronomía, con seguimiento de eventos astronómicos. Está dirigido hacia un público amateur amante de la astronomía. Utilizará las tecnologías Ionic, Firebase y Angular. La aplicación permitirá a los usuarios registrarse, iniciar sesión y compartir publicaciones relacionadas con la astronomía. También incluirá características como comentarios, likes y seguimiento de otros usuarios. Además, ofrecerá la oportunidad de retar al resto de usuarios para lograr visualizar eventos astronómicos, principalmente locales.

## Tecnologías utilizadas

- Ionic: Framework para el desarrollo de aplicaciones móviles híbridas.
- Firebase: Plataforma de Google para el desarrollo de aplicaciones web y móviles.
- Angular: Framework para el desarrollo de aplicaciones web.

## Diseño y Prototipo

### Wireframes

![Wireframes](documentation/figma/figma_wireframe.png)

Puedes ver los wireframes en [Figma](<https://www.figma.com/file/w9Uwd2Dva3bZ8ZnCWy2Se5/WireGen---AI-GPT-wireframe-generation-(Community)?type=design&node-id=0-1&mode=design>).

### Prototipo

![Prototipo](documentation/figma/figma_prototipo.png)

Puedes interactuar con el prototipo en [Figma](<https://www.figma.com/file/qQsY4Euu1dYrlZAghxVxjW/Untitled?type=design&node-id=0-1&mode=design)>).

## Instalación

1. Clona este repositorio en tu máquina local:

```bash
git clone https://gitlab.com/estudia1/dam_proyecto.git
```

2. Navega al directorio del proyecto:

```bash
cd dam_proyecto
```

3. Instala las dependencias necesarias:

```bash
npm install
```

4. Configura Firebase según las instrucciones proporcionadas en el archivo `src/environments/environment.ts`.

5. Inicia la aplicación en modo de desarrollo:

```bash
ionic serve
```

## Contribución

Si deseas contribuir a este proyecto, sigue los siguientes pasos:

1. Haz un fork de este repositorio.
2. Crea una rama nueva para tu función o corrección de errores: `git checkout -b mi-nueva-funcion`
3. Realiza los cambios y commit: `git commit -am 'Añadida nueva función'`
4. Envía tus cambios a tu repositorio fork: `git push origin mi-nueva-funcion`
5. Crea una nueva solicitud de extracción (Pull Request) en este repositorio.

## Licencia

Este proyecto está bajo la Licencia MIT. Consulta el archivo [LICENSE](LICENSE) para más detalles.
