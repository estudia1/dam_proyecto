"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_pages_perfil_perfil_module_ts"],{

/***/ 2419:
/*!**********************************************************!*\
  !*** ./src/app/pages/perfil/perfil.page.scss?ngResource ***!
  \**********************************************************/
/***/ ((module) => {

module.exports = "@charset \"UTF-8\";\n/* Estilos adicionales para la imagen dentro del ion-avatar */\nion-avatar ion-img {\n  margin-left: 1rem;\n}\nion-label {\n  font-size: 2em;\n}\n/* Estilos personalizados para hacer el botón más pequeño */\n.small-button {\n  font-size: 0.9em;\n}\n.footer-card {\n  position: fixed;\n  bottom: 0;\n}\n/* Estilos por defecto para pantallas mayores a 360px */\n.footer-card ion-button {\n  --min-width: 0;\n  --max-width: 100%;\n  --padding-start: 0.5rem;\n  --padding-end: 0.5rem;\n  --padding-top: 0;\n  --padding-bottom: 0;\n}\nion-label.custom-size {\n  font-size: 1.4em;\n}\nion-label.email-label {\n  display: none;\n  font-size: 1.4em;\n}\n/* Estilos predeterminados para móviles aquí */\n/* Media query para pantallas pequeñas (generalmente móviles) */\n@media only screen and (max-width: 600px) {\n  .footer-card ion-button {\n    --min-width: 50px; /* Ancho mínimo del botón */\n    --max-width: 80px; /* Ancho máximo del botón */\n    --padding-start: 5px; /* Espaciado interno a la izquierda */\n    --padding-end: 5px; /* Espaciado interno a la derecha */\n    --padding-top: 5px; /* Espaciado interno arriba */\n    --padding-bottom: 5px; /* Espaciado interno abajo */\n  }\n  ion-avatar ion-img {\n    margin-left: 0;\n  }\n  .small-button {\n    font-size: 0.55em;\n    --min-width: 50%; /* Ancho mínimo del botón */\n    --max-width: 50%; /* Ancho máximo del botón */\n  }\n  ion-label.custom-mail {\n    display: none; /* Oculta la dirección de correo electrónico */\n  }\n  ion-label.email-label {\n    display: block;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBlcmZpbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0JBQWdCO0FBQWhCLDZEQUFBO0FBQ0E7RUFDRSxpQkFBQTtBQUVGO0FBQUE7RUFDRSxjQUFBO0FBR0Y7QUFBQSwyREFBQTtBQUNBO0VBQ0UsZ0JBQUE7QUFHRjtBQUFBO0VBQ0UsZUFBQTtFQUNBLFNBQUE7QUFHRjtBQUFBLHVEQUFBO0FBQ0E7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSx1QkFBQTtFQUNBLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQUdGO0FBREE7RUFDRSxnQkFBQTtBQUlGO0FBREE7RUFDRSxhQUFBO0VBQ0EsZ0JBQUE7QUFJRjtBQURBLDhDQUFBO0FBRUEsK0RBQUE7QUFDQTtFQUNFO0lBQ0UsaUJBQUEsRUFBQSwyQkFBQTtJQUNBLGlCQUFBLEVBQUEsMkJBQUE7SUFDQSxvQkFBQSxFQUFBLHFDQUFBO0lBQ0Esa0JBQUEsRUFBQSxtQ0FBQTtJQUNBLGtCQUFBLEVBQUEsNkJBQUE7SUFDQSxxQkFBQSxFQUFBLDRCQUFBO0VBR0Y7RUFBQTtJQUNFLGNBQUE7RUFFRjtFQUNBO0lBQ0UsaUJBQUE7SUFDQSxnQkFBQSxFQUFBLDJCQUFBO0lBQ0EsZ0JBQUEsRUFBQSwyQkFBQTtFQUNGO0VBRUE7SUFDRSxhQUFBLEVBQUEsOENBQUE7RUFBRjtFQUVBO0lBQ0UsY0FBQTtFQUFGO0FBQ0YiLCJmaWxlIjoicGVyZmlsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIEVzdGlsb3MgYWRpY2lvbmFsZXMgcGFyYSBsYSBpbWFnZW4gZGVudHJvIGRlbCBpb24tYXZhdGFyICovXHJcbmlvbi1hdmF0YXIgaW9uLWltZyB7XHJcbiAgbWFyZ2luLWxlZnQ6IDFyZW07XHJcbn1cclxuaW9uLWxhYmVsIHtcclxuICBmb250LXNpemU6IDJlbTtcclxufVxyXG5cclxuLyogRXN0aWxvcyBwZXJzb25hbGl6YWRvcyBwYXJhIGhhY2VyIGVsIGJvdMOzbiBtw6FzIHBlcXVlw7FvICovXHJcbi5zbWFsbC1idXR0b24ge1xyXG4gIGZvbnQtc2l6ZTogMC45ZW07XHJcbn1cclxuXHJcbi5mb290ZXItY2FyZCB7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIGJvdHRvbTogMDtcclxufVxyXG5cclxuLyogRXN0aWxvcyBwb3IgZGVmZWN0byBwYXJhIHBhbnRhbGxhcyBtYXlvcmVzIGEgMzYwcHggKi9cclxuLmZvb3Rlci1jYXJkIGlvbi1idXR0b24ge1xyXG4gIC0tbWluLXdpZHRoOiAwO1xyXG4gIC0tbWF4LXdpZHRoOiAxMDAlO1xyXG4gIC0tcGFkZGluZy1zdGFydDogMC41cmVtO1xyXG4gIC0tcGFkZGluZy1lbmQ6IDAuNXJlbTtcclxuICAtLXBhZGRpbmctdG9wOiAwO1xyXG4gIC0tcGFkZGluZy1ib3R0b206IDA7XHJcbn1cclxuaW9uLWxhYmVsLmN1c3RvbS1zaXplIHtcclxuICBmb250LXNpemU6IDEuNGVtO1xyXG59XHJcblxyXG5pb24tbGFiZWwuZW1haWwtbGFiZWwge1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbiAgZm9udC1zaXplOiAxLjRlbTtcclxufVxyXG5cclxuLyogRXN0aWxvcyBwcmVkZXRlcm1pbmFkb3MgcGFyYSBtw7N2aWxlcyBhcXXDrSAqL1xyXG5cclxuLyogTWVkaWEgcXVlcnkgcGFyYSBwYW50YWxsYXMgcGVxdWXDsWFzIChnZW5lcmFsbWVudGUgbcOzdmlsZXMpICovXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNjAwcHgpIHtcclxuICAuZm9vdGVyLWNhcmQgaW9uLWJ1dHRvbiB7XHJcbiAgICAtLW1pbi13aWR0aDogNTBweDsgLyogQW5jaG8gbcOtbmltbyBkZWwgYm90w7NuICovXHJcbiAgICAtLW1heC13aWR0aDogODBweDsgLyogQW5jaG8gbcOheGltbyBkZWwgYm90w7NuICovXHJcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDVweDsgLyogRXNwYWNpYWRvIGludGVybm8gYSBsYSBpenF1aWVyZGEgKi9cclxuICAgIC0tcGFkZGluZy1lbmQ6IDVweDsgLyogRXNwYWNpYWRvIGludGVybm8gYSBsYSBkZXJlY2hhICovXHJcbiAgICAtLXBhZGRpbmctdG9wOiA1cHg7IC8qIEVzcGFjaWFkbyBpbnRlcm5vIGFycmliYSAqL1xyXG4gICAgLS1wYWRkaW5nLWJvdHRvbTogNXB4OyAvKiBFc3BhY2lhZG8gaW50ZXJubyBhYmFqbyAqL1xyXG4gIH1cclxuXHJcbiAgaW9uLWF2YXRhciBpb24taW1nIHtcclxuICAgIG1hcmdpbi1sZWZ0OiAwO1xyXG4gIH1cclxuXHJcbiAgLnNtYWxsLWJ1dHRvbiB7XHJcbiAgICBmb250LXNpemU6IDAuNTVlbTtcclxuICAgIC0tbWluLXdpZHRoOiA1MCU7IC8qIEFuY2hvIG3DrW5pbW8gZGVsIGJvdMOzbiAqL1xyXG4gICAgLS1tYXgtd2lkdGg6IDUwJTsgLyogQW5jaG8gbcOheGltbyBkZWwgYm90w7NuICovXHJcbiAgfVxyXG5cclxuICBpb24tbGFiZWwuY3VzdG9tLW1haWwge1xyXG4gICAgZGlzcGxheTogbm9uZTsgLyogT2N1bHRhIGxhIGRpcmVjY2nDs24gZGUgY29ycmVvIGVsZWN0csOzbmljbyAqL1xyXG4gIH1cclxuICBpb24tbGFiZWwuZW1haWwtbGFiZWwge1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgfVxyXG59XHJcbiJdfQ== */";

/***/ }),

/***/ 5246:
/*!**********************************************************!*\
  !*** ./src/app/pages/perfil/perfil.page.html?ngResource ***!
  \**********************************************************/
/***/ ((module) => {

module.exports = "<ion-header class=\"ion-no-border\" translucent>\r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n      <ion-back-button defaultHref=\"/\" color=\"primary\"></ion-back-button>\r\n    </ion-buttons>\r\n    <ion-row>\r\n      <ion-title class=\"ion-text-capitalize\" *ngIf=\"usuarioLogado?.ROL\">\r\n        Hola {{usuarioLogado.NOMBRE}} !!!\r\n      </ion-title>\r\n    </ion-row>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <ion-grid class=\"ion-height-full ion-align-items-center\">\r\n    <ion-row class=\"ion-justify-content-center\">\r\n      <ion-col size-lg=\"6\" size-md=\"8\" size-xs=\"12\">\r\n        <ion-card class=\"ion-text-center\">\r\n          <ion-header>\r\n            <ion-toolbar color=\"secondary\">\r\n              <ion-title color=\"primary\">EDITAR PERFIL</ion-title>\r\n            </ion-toolbar>\r\n          </ion-header>\r\n          <ion-card-content>\r\n            <ion-row>\r\n              <ion-col size=\"12\">\r\n                <ion-item>\r\n                  <ion-label\r\n                    class=\"email-label\"\r\n                    size=\"large\"\r\n                    *ngIf=\"usuarioLogado?.EMAIL\"\r\n                    >{{usuarioLogado.EMAIL}}</ion-label\r\n                  >\r\n                </ion-item>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-row>\r\n              <ion-col size=\"6\">\r\n                <ion-item>\r\n                  <ion-avatar class=\"avatar\">\r\n                    <ion-img\r\n                      [src]=\"usuarioLogado?.AVATAR\"\r\n                      class=\"imagenAvatar\"\r\n                    ></ion-img>\r\n                  </ion-avatar>\r\n                </ion-item>\r\n                <ion-item>\r\n                  <ion-button\r\n                    (click)=\"avatarActionSheet()\"\r\n                    expand=\"none\"\r\n                    fill=\"outline\"\r\n                    shape=\"none\"\r\n                    class=\"small-button\"\r\n                  >\r\n                    Cambiar Avatar\r\n                  </ion-button>\r\n                </ion-item>\r\n              </ion-col>\r\n              <ion-col size=\"6\">\r\n                <ion-list>\r\n                  <ion-item>\r\n                    <ion-label class=\"custom-size\" *ngIf=\"usuarioLogado?.ROL\"\r\n                      >Score: {{usuarioLogado.PUNTOS}}</ion-label\r\n                    >\r\n                    <!-- Contenido del email -->\r\n                  </ion-item>\r\n                  <ion-item>\r\n                    <ion-label class=\"custom-size\" *ngIf=\"usuarioLogado?.ROL\"\r\n                      >{{usuarioLogado.NOMBRE}}</ion-label\r\n                    >\r\n                    <!-- Contenido del nombre -->\r\n                  </ion-item>\r\n                  <ion-item>\r\n                    <ion-label\r\n                      class=\"custom-mail custom-size\"\r\n                      *ngIf=\"usuarioLogado?.EMAIL\"\r\n                      >{{usuarioLogado.EMAIL}}</ion-label\r\n                    >\r\n                    <!-- Contenido del email -->\r\n                  </ion-item>\r\n                </ion-list>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-card-content>\r\n        </ion-card>\r\n      </ion-col>\r\n    </ion-row>\r\n    <ion-footer class=\"footer-card\">\r\n      <ion-row class=\"ion-justify-content-center\">\r\n        <ion-col size-lg=\"6\" size-md=\"8\" size-xs=\"12\">\r\n          <ion-card>\r\n            <ion-card-content>\r\n              <ion-row>\r\n                <ion-col size=\"6\" class=\"ion-text-start\">\r\n                  <!-- Botón de cerrar sesión -->\r\n                  <ion-button\r\n                    (click)=\"logOut()\"\r\n                    expand=\"none\"\r\n                    fill=\"outline\"\r\n                    shape=\"none\"\r\n                  >\r\n                    Cerrar sesión\r\n                  </ion-button>\r\n                </ion-col>\r\n\r\n                <ion-col size=\"6\" class=\"ion-text-end\">\r\n                  <!--Zona de peligro y botón de eliminar cuenta -->\r\n                  <ion-button\r\n                    (click)=\"alertDelete()\"\r\n                    expand=\"none\"\r\n                    fill=\"solid\"\r\n                    shape=\"none\"\r\n                    color=\"danger\"\r\n                  >\r\n                    Eliminar Cuenta\r\n                  </ion-button>\r\n                </ion-col>\r\n              </ion-row>\r\n            </ion-card-content>\r\n          </ion-card>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-footer>\r\n  </ion-grid>\r\n</ion-content>\r\n";

/***/ }),

/***/ 8911:
/*!*******************************************************!*\
  !*** ./src/app/pages/perfil/perfil-routing.module.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PerfilPageRoutingModule": () => (/* binding */ PerfilPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _perfil_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./perfil.page */ 7992);




const routes = [
    {
        path: '',
        component: _perfil_page__WEBPACK_IMPORTED_MODULE_0__.PerfilPage
    }
];
let PerfilPageRoutingModule = class PerfilPageRoutingModule {
};
PerfilPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], PerfilPageRoutingModule);



/***/ }),

/***/ 6217:
/*!***********************************************!*\
  !*** ./src/app/pages/perfil/perfil.module.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PerfilPageModule": () => (/* binding */ PerfilPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 6362);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _perfil_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./perfil-routing.module */ 8911);
/* harmony import */ var _perfil_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./perfil.page */ 7992);
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/components.module */ 5642);








let PerfilPageModule = class PerfilPageModule {
};
PerfilPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _perfil_routing_module__WEBPACK_IMPORTED_MODULE_0__.PerfilPageRoutingModule,
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_2__.ComponentsModule
        ],
        declarations: [_perfil_page__WEBPACK_IMPORTED_MODULE_1__.PerfilPage]
    })
], PerfilPageModule);



/***/ }),

/***/ 7992:
/*!*********************************************!*\
  !*** ./src/app/pages/perfil/perfil.page.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PerfilPage": () => (/* binding */ PerfilPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _perfil_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./perfil.page.html?ngResource */ 5246);
/* harmony import */ var _perfil_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./perfil.page.scss?ngResource */ 2419);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/platform-browser */ 318);
/* harmony import */ var _capacitor_camera__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @capacitor/camera */ 4241);
/* harmony import */ var src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/auth.service */ 7556);
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/user.service */ 3071);
/* harmony import */ var src_app_services_avisos_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/avisos.service */ 9595);
/* harmony import */ var _services_multimedia_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/multimedia.service */ 4579);













let PerfilPage = class PerfilPage {
    /**
     * Constructor de clase.
     * Revisa el estado de la sesión, recoge el mail y busca su perfil para cargar los datos.
     * @param authSvc Servicio de autenticación para el manejo del cambio de contraseña
     * @param actionSheetCtrl Controlador del ActionSheet que mostrará los accesos a la Camara o Galeria
     * @param alertCtrl Controlador de las Alertas
     * @param userSvc Servicio de manejo del Usuario
     * @param router Controlador de rutas de accceso
     * @param navCtrl Controlador para redirigir a determinadas rutas
     * @param avisosSvc Servicio de avisos a traves de Toast
     * @param multimediaSvc Servicio de manejo de las imagenes
     * @param sanitizer Libreria que 'sanea' la imagen para hacer una subida segura a Firestorage
     */
    constructor(authSvc, actionSheetCtrl, alertCtrl, userSvc, router, navCtrl, avisosSvc, multimediaSvc, sanitizer) {
        this.authSvc = authSvc;
        this.actionSheetCtrl = actionSheetCtrl;
        this.alertCtrl = alertCtrl;
        this.userSvc = userSvc;
        this.router = router;
        this.navCtrl = navCtrl;
        this.avisosSvc = avisosSvc;
        this.multimediaSvc = multimediaSvc;
        this.sanitizer = sanitizer;
        this.authSvc.initAuthStateListener();
        this.userEmail = this.authSvc.userEmail;
    }
    /**
     * Metodo de inicio
     */
    ngOnInit() {
        this.userSvc.getUserByEmail(this.userEmail).subscribe((usuario) => {
            this.usuarioLogado = usuario;
        });
    }
    /**
     * Metodo para deslogarse. Llama al servicio de Autenticacion, desloga, y te envia al Home.
     */
    logOut() {
        this.authSvc.signOut();
        this.navCtrl.navigateRoot('/home');
    }
    /**
     * Método que muestra un ActionSheet con los iconos de Camara y Galeria para seleccionar la imagen del Avatar
     */
    avatarActionSheet() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            this.actionSheet = yield this.actionSheetCtrl.create({
                cssClass: 'multimedia-class',
                buttons: [
                    {
                        icon: 'camera-outline',
                        handler: () => {
                            this.setAvatarCamara();
                        },
                    },
                    {
                        icon: 'image-outline',
                        handler: () => {
                            this.setAvatarGallery();
                        },
                    },
                    {
                        text: 'Cancel',
                        icon: 'close',
                        role: 'cancel',
                    },
                ],
                /** Template personalizado para mostrar los botones a la izquierda y derecha
                 * Los botones se ajustarán a los lados opuestos de la hoja de acción
                 * utilizando la clase CSS "ion-action-sheet-buttons-start" e "ion-action-sheet-buttons-end"
                 * y se alinearán al centro de la hoja de acción utilizando la clase CSS "ion-justify-content-center"
                 */
                backdropDismiss: true,
                animated: true,
                keyboardClose: true,
                mode: 'ios',
                translucent: true,
                id: 'my-action-sheet',
            });
            yield this.actionSheet.present();
        });
    }
    /**
     * Metodo que muestra una Alerta al eliminar el Usuario y si aceptamos, lo ELIMINA. NO IMPLEMENTADO
     * Al eliminarlo de devuelve al Home
     * @param documentId ID del usuario a eliminar (el mismo que ha iniciado sesión)
     */
    alertDelete() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertCtrl.create({
                header: 'Eliminar usuario',
                message: '¿Estás seguro de que quieres eliminar este usuario?',
                buttons: [
                    {
                        text: 'Cancelar',
                        role: 'cancel',
                        cssClass: 'secondary',
                    },
                    {
                        text: 'Eliminar',
                        handler: () => {
                            this.authSvc.deleteAccount();
                            this.router.navigateByUrl('/home');
                        },
                    },
                ],
            });
            yield alert.present();
        });
    }
    /**
     * Muestra una Alerta para cambiar la contraseña. Se especifica 2 veces la contraseña,
     * y si coinciden lanza el servicio de cambio de contraseña y te desloga para que entres
     * con la contraseña nueva
     */
    alertPassChange() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertCtrl.create({
                header: 'Pon tu nueva contraseña',
                buttons: [
                    {
                        text: 'Cancelar',
                        role: 'cancel',
                        handler: () => { },
                    },
                    {
                        text: 'Aceptar',
                        handler: (data) => {
                            // Obtener los valores de los campos de texto
                            const password1 = data.password1;
                            const password2 = data.password2;
                            console.log(password1, password2);
                            // Comparar los valores de las contraseñas y mostrar un mensaje de error si no coinciden
                            if (password1 != password2) {
                                this.avisosSvc.presentToast('Las contraseñas no coinciden', 'danger');
                            }
                            else {
                                // Las contraseñas coinciden, realizar la acción deseada
                                this.authSvc.passChange(password1);
                                this.logOut();
                                this.avisosSvc.presentToast('Contraseña cambiada correctamente', 'success');
                            }
                        },
                    },
                ],
                inputs: [
                    {
                        name: 'password1',
                        type: 'password',
                        placeholder: 'Pon tu nueva contraseña',
                        attributes: {
                            minlength: 6,
                        },
                    },
                    {
                        name: 'password2',
                        type: 'password',
                        placeholder: 'Repite la contraseña',
                        attributes: {
                            minlength: 6,
                        },
                    },
                ],
            });
            yield alert.present();
        });
    }
    /**
     * Metodo para aplicar un Avatar al Usuario desde la Galeria
     * Se selecciona la imagen de la galeria, la sanea para que sea segura y lanza el servicio de subida de la imagen
     * a Firestorage y posteriormente el servicio de actualizacion del usuario pasando el ID del usuario y la URL de acceso
     */
    setAvatarGallery() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            const foto = yield _capacitor_camera__WEBPACK_IMPORTED_MODULE_2__.Camera.getPhoto({
                quality: 90,
                allowEditing: false,
                resultType: _capacitor_camera__WEBPACK_IMPORTED_MODULE_2__.CameraResultType.DataUrl,
                source: _capacitor_camera__WEBPACK_IMPORTED_MODULE_2__.CameraSource.Photos,
            });
            //hay que sanearla
            this.image = this.sanitizer.bypassSecurityTrustResourceUrl(foto && foto.dataUrl);
            let blob = yield fetch(foto.dataUrl).then((r) => r.blob());
            this.imagenSaneada = blob;
            const res = yield this.multimediaSvc.subirImagen(this.imagenSaneada, 'avatar', this.usuarioLogado.EMAIL);
            console.log('ruta', res);
            console.log('id', this.usuarioLogado.ID);
            this.userSvc.updateUserAvatar(this.usuarioLogado.ID, res);
        });
    }
    /**
     * Metodo para aplicar un Avatar al Usuario desde la Camara
     * Se selecciona la imagen de la galeria, la sanea para que sea segura y lanza el servicio de subida de la imagen
     * a Firestorage y posteriormente el servicio de actualizacion del usuario pasando el ID del usuario y la URL de acceso
     */
    setAvatarCamara() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            const foto = yield _capacitor_camera__WEBPACK_IMPORTED_MODULE_2__.Camera.getPhoto({
                quality: 90,
                allowEditing: false,
                resultType: _capacitor_camera__WEBPACK_IMPORTED_MODULE_2__.CameraResultType.DataUrl,
                source: _capacitor_camera__WEBPACK_IMPORTED_MODULE_2__.CameraSource.Camera,
            });
            //hay que sanearla
            this.image = this.sanitizer.bypassSecurityTrustResourceUrl(foto && foto.dataUrl);
            let blob = yield fetch(foto.dataUrl).then((r) => r.blob());
            this.imagenSaneada = blob;
            const res = yield this.multimediaSvc.subirImagen(this.imagenSaneada, 'avatar', this.usuarioLogado.EMAIL);
            console.log('ruta', res);
            console.log('id', this.usuarioLogado.ID);
            this.userSvc.updateUserAvatar(this.usuarioLogado.ID, res);
        });
    }
};
PerfilPage.ctorParameters = () => [
    { type: src_app_services_auth_service__WEBPACK_IMPORTED_MODULE_3__.AuthService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.ActionSheetController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.AlertController },
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_4__.UserService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_9__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.NavController },
    { type: src_app_services_avisos_service__WEBPACK_IMPORTED_MODULE_5__.AvisosService },
    { type: _services_multimedia_service__WEBPACK_IMPORTED_MODULE_6__.MultimediaService },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_10__.DomSanitizer }
];
PerfilPage = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_11__.Component)({
        selector: 'app-perfil',
        template: _perfil_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_perfil_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], PerfilPage);



/***/ })

}]);
//# sourceMappingURL=src_app_pages_perfil_perfil_module_ts.js.map